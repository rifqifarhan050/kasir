from django.apps import AppConfig


class AppAplikasiKasirConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_aplikasi_kasir'
