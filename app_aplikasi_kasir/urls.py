from django.urls import path
from .views import *

urlpatterns = [
    
    path('',login.Login ,name='login'),

    path('logout',login.Logout,name='logout'),

    path('form_pelanggan',pelanggan.Read_pelanggan,name='read_pelanggan'),

    path('input_pelanggan',pelanggan.Input_pelanggan,name='input_pelanggan'),

    path('update_pelanggan/<int:id_pelanggan>',pelanggan.Update_pelanggan,name='update_pelanggan'),

    path('hapus_pelanggan/<int:id_pelanggan>',pelanggan.Delete_pelanggan,name='hapus_pelanggan'),

    path('form_user',user.Read_user,name='read_user'),

    path('input_user',user.login_user,name='input_user'),

    path('update_user/<int:id_user>',user.Update_user,name='update_user'),

    path('delete_user/<int:id_user>',user.Delete_user,name='delete_user'),

    path('form_produk',produk.Read_produk,name='read_produk'),

    path('input_produk',produk.Input_produk,name='input_produk'),

    path('update_produk/<int:id_produk>',produk.Update_produk,name='update_produk'),

    path('delete_produk/<int:id_produk>',produk.Delete_produk,name='hapus_produk'),

    path('form_penjualan',penjualan.Read_penjualan,name='read_penjualan'),

    path('tambah_penjualan',penjualan.Input_pelanggan,name='input_penjualan'),
    
    path('lap_pelanggan',laporan.laporan_pelanggan,name='lap_pelanggan'),

    path('form_icon',icon.Read_icon,name='read_icon'),

    path('input_icon',icon.Input_icon,name='input_icon'),

    path('delete_icon/<int:id_icon>',icon.Delete_icon,name='delete_icon'),

    path('frontend/',frontend.index,name='home_frontend'),

    path('blog',blog.read_blog,name='read_blog'),

    path('input_blog',blog.input_blog,name='input_blog'),

    path('delete_blog/<int:id_blog>',blog.Delete_blog,name='delete_blog'),




    #home
    path('home/', home.index,name='home'),
]