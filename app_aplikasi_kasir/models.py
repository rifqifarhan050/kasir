from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, Group, Permission
from django.contrib.auth.base_user import BaseUserManager
from django.utils import timezone
from django.utils.translation import gettext_lazy as _



class CustomUserManager(BaseUserManager):
    def create_user(self, email, password=None, **extra_fields):
        if not email:
            raise ValueError(_("The Email must be set"))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)
        extra_fields.setdefault('is_verified', True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError(_("Superuser must have is_staff=True."))
        if extra_fields.get("is_superuser") is not True:
            raise ValueError(_("Superuser must have is_superuser=True."))
        if extra_fields.get('is_verified') is not True:
            raise ValueError(_("Superuser must have is_verified=True."))
        return self.create_user(email, password, **extra_fields)

class Account(AbstractBaseUser, PermissionsMixin):
    id_user = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=40)
    email = models.EmailField(_("email address"), unique=True)
    hak_akses = models.CharField(max_length=30, default="-")
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_verified = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email

    groups = models.ManyToManyField(Group, related_name="user_set", blank=True)
    user_permissions = models.ManyToManyField(Permission, related_name="user_set", blank=True)

class user_tb(models.Model):
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)

class pelangan_tb(models.Model):
    nama_pelanggan = models.CharField(max_length=255)
    alamat_pelanggan = models.CharField(max_length=255)
    no_telp_pelanggan = models.CharField(max_length=20)

class produk_tb(models.Model):
    nama_produk = models.CharField(max_length=255)
    harga_produk = models.CharField(max_length=255)
    gambar = models.ImageField(upload_to='produk/', null=True, default="img/produk.jpg")  # upload_to untuk menempatkan file yang kita upload di taruh di mana
    jml_stok_produk = models.IntegerField()

class penjualan_tb(models.Model):
    pelanggan = models.ForeignKey(pelangan_tb,on_delete=models.RESTRICT,default=None)
    barang = models.JSONField(null=True)
    total_harga = models.CharField(max_length=255, null=True)
    pembayaran = models.CharField(max_length=255,null=True)
    kembalian = models.CharField(max_length=255, null=True)
    tgl_penjualan = models.DateField()

class icon_pelayanan_tb(models.Model):
    icon = models.ImageField(upload_to='icon/', null=True, default="img/icon.jpg")
    judul_icon = models.CharField(max_length=255)
    deskripsi_icon = models.TextField()

class blog_tb(models.Model):
    gambar_blog = models.ImageField(upload_to='blog/', null=True, default="img/blog.jpg")
    judul_blog = models.CharField(max_length=255)
    isi_blog = models.TextField()

    




# Create your models here.
