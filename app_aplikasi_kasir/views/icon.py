from django.shortcuts import render,redirect
from django.http import HttpResponse
from ..models import icon_pelayanan_tb

def Read_icon(request):
    data =  icon_pelayanan_tb.objects.all()
    return render(request, 'icon/index.html',{'data' : data})

def Input_icon(request):
    if request.method == 'GET':
        return render(request, 'icon/index.html')

    if request.method == 'POST':
        gambar_icon = request.FILES.get('gambar_icon')
        judul_icon = request.POST.get('judul_icon')
        deskripsi_icon = request.POST.get('deskripsi_icon')

        table = icon_pelayanan_tb()

        if table is not None :
            table.icon = gambar_icon
            table.judul_icon = judul_icon
            table.deskripsi_icon = deskripsi_icon

            table.save()
            return redirect('read_icon')

def Delete_icon(request,id_icon):
    data = icon_pelayanan_tb.objects.filter(id = id_icon)
    data.delete()
    return redirect('read_icon')