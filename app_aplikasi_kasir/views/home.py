from django.shortcuts import render,redirect
from django.http import HttpResponse
from ..models import produk_tb

def index(request):
    data = produk_tb.objects.all()
    return render(request,'home/index.html',{'data' : data})