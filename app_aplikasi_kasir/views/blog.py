from django.shortcuts import render,redirect
from django.http import HttpResponse
from ..models import blog_tb

def read_blog(request):
    data = blog_tb.objects.all()
    return render(request,'blog/index.html',{'data' : data})


def input_blog(request):
    if request.method == 'POST':
        gambar_blog = request.FILES.get('gambar_blog')
        judul_blog = request.POST.get('judul_blog')
        isi_blog = request.POST.get('isi_blog')

        table = blog_tb()

        if table is not None :
            table.gambar_blog = gambar_blog
            table.judul_blog = judul_blog
            table.isi_blog = isi_blog

            table.save()
            return redirect('read_blog')
    else :
        return HttpResponse('<html><script>alert("Terjadi Kesalahan"):window.location="";</script></html>')
    
def Delete_blog(request,id_blog):
    data = blog_tb.objects.filter(id = id_blog)
    data.delete()
    return redirect('read_blog')