from django.shortcuts import render,redirect
from django.http import HttpResponse
from ..models import pelangan_tb
from django.contrib.auth.decorators import login_required
from ..decorators import *
from django.views.decorators.http import require_http_methods

def laporan_pelanggan(request):
    data = pelangan_tb.objects.all()
    return render(request,'laporan/lap_pelanggan.html',{'data' : data})
