from django.shortcuts import render,redirect
from django.http import HttpResponse
from ..models import user_tb

from django.views.decorators.http import require_http_methods
from ..models import Account
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, logout
from django.contrib.auth import login as auth_login
from ..decorators import *




def Login(request):
    if request.method == 'GET':
        return render(request,'login/login.html')
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')

        user = authenticate(request, email=email, password=password)
        if user is not None:
            auth_login(request, user)
            return redirect('read_pelanggan')
        else:
            return HttpResponse('<html><script>alert("Masukkan Username dan Password Dengan Benar");window.location="";</script></html>')
    

def Logout(request):
    logout(request)
    return redirect('login')


def login_user(request):
    if request.method == 'GET':
        return render(request,'login/login.html')
    if request.method == 'POST':
        username = request.POST.get('username')
        email = request.POST.get('email')
        password = request.POST.get('password')

        insert = Account()
        insert.email = email
        insert.nama = username
        insert.set_password(password)
        insert.save()

        return redirect('read_pelanggan')

        
