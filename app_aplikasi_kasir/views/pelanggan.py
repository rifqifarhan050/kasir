from django.shortcuts import render,redirect
from django.http import HttpResponse
from ..models import pelangan_tb
from django.contrib.auth.decorators import login_required
from ..decorators import *
from django.views.decorators.http import require_http_methods



@login_required
def Read_pelanggan(request):
    data = pelangan_tb.objects.all()
    return render(request,'pelanggan/form_pelanggan.html',{'data' : data})

def Input_pelanggan(request):
    if request.method == 'GET':
        return(request,'pelanggan/form_pelanggan.html')
    if request.method == 'POST':
        nama_pelanggan = request.POST['nama_pelanggan']
        alamat_pelanggan = request.POST['alamat_pelanggan']
        no_telp_pelanggan = request.POST['no_telp_pelanggan']

        table = pelangan_tb()
        if table is not None :
            table.nama_pelanggan = nama_pelanggan
            table.alamat_pelanggan = alamat_pelanggan
            table.no_telp_pelanggan = no_telp_pelanggan
            table.save()

            return redirect('read_pelanggan')
        
def Update_pelanggan(request,id_pelanggan):
    if request.method == 'GET':
        data = pelangan_tb.objects.filter(id=id_pelanggan)
        return render(request,'pelanggan/form_pelanggan.html',{'data' : data})
    if request.method == 'POST':
        nama_pelanggan = request.POST['nama_pelanggan']
        alamat_pelanggan = request.POST['alamat_pelanggan']
        no_telp_pelanggan = request.POST['no_telp_pelanggan']
        table = pelangan_tb(id=id_pelanggan)
        if table is not None :
            table.nama_pelanggan = nama_pelanggan
            table.alamat_pelanggan = alamat_pelanggan
            table.no_telp_pelanggan = no_telp_pelanggan
            table.save()
            
            return redirect('read_pelanggan')
        
def Delete_pelanggan(request,id_pelanggan):
    data = pelangan_tb.objects.filter(id = id_pelanggan)
    data.delete()
    return redirect('read_pelanggan')
    