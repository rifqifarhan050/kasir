from django.shortcuts import render,redirect
from django.http import HttpResponse
from ..models import produk_tb,icon_pelayanan_tb,blog_tb

def index(request):
    data = produk_tb.objects.all()
    data_icon = icon_pelayanan_tb.objects.all()
    data_blog = blog_tb.objects.all()
    return render(request,'layout/base_frontend.html',{'data' : data,'data_icon' : data_icon,'data_blog' : data_blog })

