from django.shortcuts import render,redirect
from django.http import HttpResponse
from ..models import user_tb, Account
from django.contrib.auth.decorators import login_required

@login_required
def Read_user(request):
    data = user_tb.objects.all()
    return render(request,'user/form_user.html',{'data' : data})

def Input_user(request):
    if request.method == 'GET':
        return(request,'user/form_user.html')
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        table = user_tb()

        if table is not None :
            table.username = username
            table.password = password
            table.save()

            return redirect('read_user')

def Update_user(request,id_user):
    if request.method == 'GET':
        data = user_tb.objects.filter(id=id_user)
        return render(request,'user/form_user.html',{'data' : data })
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        table = user_tb(id=id_user)

        if table is not None :
            table.username = username
            table.password = password

            table.save()

            return redirect('read_user')
        
def Delete_user(request,id_user):
    data = user_tb.objects.filter(id=id_user)
    data.delete()
    return redirect('read_user')


def login_user(request):
    if request.method == 'GET':
        return render(request,'login/login.html')
    if request.method == 'POST':
        username = request.POST.get('username')
        email = request.POST.get('email')
        password = request.POST.get('password')

        insert = Account()
        insert.email = email
        insert.nama = username
        insert.set_password(password)
        insert.save()

        return redirect('read_pelanggan')