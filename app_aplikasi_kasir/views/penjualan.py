from django.shortcuts import render,redirect
from django.http import HttpResponse,JsonResponse
from ..models import penjualan_tb,pelangan_tb, produk_tb
from django.contrib.auth.decorators import login_required
import json

@login_required
def Read_penjualan(request):
    data = penjualan_tb.objects.all()
    data_pelanggan = pelangan_tb.objects.all()
    barang = produk_tb.objects.all()
    
    return render(request,'penjualan/form_penjualan.html',{'data' : data, 'data_pelanggan': data_pelanggan, 'barang': barang})


@login_required
def Input_pelanggan(request):
    if request.method == 'GET':
        data_pelanggan = pelangan_tb.objects.all()
        barang = produk_tb.objects.all()
        return render(request, 'penjualan/form_penjualan.html', {'data_pelanggan': data_pelanggan, 'barang': barang})

    if request.method == 'POST':
        data = {'message': 'Terjadi Kesalahan', 'status':'error'}
        try:
            tgl_penjualan = request.POST['tgl_penjualan']
            nama_pelanggan_fk = request.POST.get('nama_pelanggan_fk')
            print(nama_pelanggan_fk)
            list_barang = request.POST.get('attribut', '[]')
            list_barang = json.loads(list_barang)
            print(list_barang)

            # Proses data yang diterima
            # Simpan data ke database atau lakukan operasi lain yang diperlukan
            # Contoh: Simpan data ke database (tergantung pada model dan logika Anda)

            # Simpan data penjualan
            penjualan = penjualan_tb(
                tgl_penjualan=tgl_penjualan,
                pelanggan_id=nama_pelanggan_fk,
                barang=list_barang
            )
            penjualan.save()
            return JsonResponse(data, status=200)
        
        except Exception as e:
            print('Error input data penjualan', e)
            data['message'] = 'Reservasi Gagal!'
            data['status'] = 'error'
            return JsonResponse(data, status = 400)
            # return redirect('read_penjualan')

    return JsonResponse(data, status=405)

    
    
