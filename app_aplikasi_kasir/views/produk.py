from django.shortcuts import render,redirect
from django.http import HttpResponse
from ..models import produk_tb
from django.contrib.auth.decorators import login_required

@login_required
def Read_produk(request):
    data = produk_tb.objects.all()
    return render(request,'produk/form_produk.html',{'data' : data})

def Input_produk(request):
    if request.method == 'GET' :
        return(request,'produk/form_produk.html')
    if request.method == 'POST':
        nama_produk = request.POST['nama_produk']
        harga_produk = request.POST['harga_produk']
        jml_stok_produk = request.POST['jml_stok_produk']
        gambar = request.FILES['gambar']

        table = produk_tb()
        
        if table is not None :
            table.nama_produk = nama_produk
            table.harga_produk = harga_produk
            table.jml_stok_produk = jml_stok_produk
            table.gambar = gambar
            
            table.save()

            return redirect('read_produk')
        
def Update_produk(request,id_produk):
    if request.method == 'GET':
        data = produk_tb.objects.filter(id=id_produk)
        return(request,'produk/form_produk.html',{'data' : data})
    if request.method == 'POST':
        nama_produk = request.POST['nama_produk']
        harga_produk = request.POST['harga_produk']
        jml_stok_produk = request.POST['jml_stok_produk']
        table = produk_tb(id=id_produk)
        
        if table is not None:
            table.nama_produk = nama_produk
            table.harga_produk = harga_produk
            table.jml_stok_produk = jml_stok_produk
            table.save()

            return redirect('read_produk')

def Delete_produk(request,id_produk):
    data = produk_tb.objects.filter(id = id_produk)
    data.delete()
    return redirect('read_produk')