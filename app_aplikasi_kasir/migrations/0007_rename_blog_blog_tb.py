# Generated by Django 5.0.6 on 2024-06-19 18:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_aplikasi_kasir', '0006_blog'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='blog',
            new_name='blog_tb',
        ),
    ]
