# Generated by Django 5.0.6 on 2024-06-19 18:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_aplikasi_kasir', '0005_rename_judul_icon_icon_pelayanan_tb_judul_icon'),
    ]

    operations = [
        migrations.CreateModel(
            name='blog',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gambar_blog', models.ImageField(default='img/blog.jpg', null=True, upload_to='blog/')),
                ('judul_blog', models.CharField(max_length=255)),
                ('isi_blog', models.TextField()),
            ],
        ),
    ]
